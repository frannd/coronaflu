import 'package:flutter/material.dart';
import 'package:corona_flu/app/services/api_service.dart';
import 'package:google_fonts/google_fonts.dart';

class GridDashboard extends StatelessWidget {

  List<Item> myList;


  GridDashboard({this.myList});


  @override
  Widget build(BuildContext context) {
    //List<Item> Items;
    var color = 0xff453658;
    return Flexible(
      child: GridView.count(
        childAspectRatio: 1.0,
        padding: EdgeInsets.all(16),
        crossAxisCount: 2,
        crossAxisSpacing: 18,
        mainAxisSpacing: 18,
        children: myList.map((data){
          return Container(
            decoration: BoxDecoration(color: Color(color), borderRadius: BorderRadius.circular(10)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //Image.asset(data.img, width: 42,),
                IconButton(
                  icon: data.img,
                ),
                SizedBox(
                  height: 14,
                ),
                Text(data.title, style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w600
                  ),
                ),),
                SizedBox(
                  height: 10,
                ),
                 Text(data.subtitle.toString(), style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                    color: Colors.white38,
                    fontSize: 14,
                    fontWeight: FontWeight.w600
                  ),
                ),),
              ],
            ),
          );
        }).toList()
      ),
    );
  }




}

class Item{
  String title;
  int subtitle;
  Icon img;

  Item({this.title, this.subtitle, this.img});
}


