import 'package:corona_flu/app/services/api_service.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'app/services/api.dart';
import 'griddashboard.dart';


void main() => runApp(MaterialApp(home: MyHomePage()));






class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}




class _MyHomePageState extends State<MyHomePage> {
  
  String _accessToken = "";
  int _cases = 0;
  int _deaths = 0;
  int _recovered = 0;
  int _suspected = 0;
  int _confirmed = 0;


/* Método que trae response de la API */
  void _updateAccessToken() async {

    final apiService = APIService(API.sandbox());
    final accessToken = await apiService.getAccessToken();

    final cases = await apiService.getEndPointData(
      accessToken: accessToken, 
      endpoint: Endpoint.cases,
    );

    final deaths = await apiService.getEndPointData(
      accessToken: accessToken, 
      endpoint: Endpoint.deaths,
    );

    final recovered = await apiService.getEndPointData(
      accessToken: accessToken, 
      endpoint: Endpoint.recovered,
    );

    final suspected = await apiService.getEndPointData(
      accessToken: accessToken, 
      endpoint: Endpoint.casesSuspected
    );

    final confirmed = await apiService.getEndPointData(
      accessToken: accessToken, 
      endpoint: Endpoint.casesConfirmed
    );

    setState(() {
      _accessToken = accessToken;
      _cases = cases;
      _deaths = deaths;
      _recovered = recovered;
      _suspected = suspected;
      _confirmed = confirmed;
    });
  }

  @override
  Widget build(BuildContext context) {
    
    Item item1 = new Item(
        title: "Casos",
        subtitle: _cases,
        img: Icon(
          Icons.adb,
          color: Colors.green,
      )
      );

    Item item2 = new Item(
      title: "Muertes",
      subtitle: _deaths,
      img: Icon(
        Icons.block,
        color: Colors.red,
      )
    );

    Item item3 = new Item(
      title: "Recuperados!",
      subtitle: _recovered,
      img: Icon(
        Icons.accessibility_new,
        color: Colors.white,
      )
    );

    return Scaffold(
      backgroundColor: Color(0xff392850),
      body: Column(children: <Widget>[
        SizedBox(height: 110,),
        Padding(
          padding: EdgeInsets.only(left: 16, right: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Coronaflu", style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold
                    )
                  ),),
                  SizedBox(height: 4,),
                   Text("Coronavirus App", style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                      color: Color(0xffa29aac),
                      fontSize: 14,
                      fontWeight: FontWeight.w600
                    )
                  ),),
                ],
              ),
              IconButton(
                alignment: Alignment.topCenter,
                /*icon: Image.asset(
                  "assets/corona.png", 
                  width: 200,
                ),*/
                icon: Icon(Icons.add_alert),
                onPressed: _updateAccessToken,
              )
            ],
           ),
          ),
          SizedBox(
            height: 40,
          ),
          GridDashboard(
            myList: [item1,item2,item3],
          )
        ],
      ),
    );
  }
}






/*
appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              'accessToken: $_accessToken',
              style: Theme.of(context).textTheme.display1,
            ),
            if(_cases != null)
              Text(
              'cases: $_cases',
              style: Theme.of(context).textTheme.display1,
              ),
              Text(
              'deaths: $_deaths',
              style: Theme.of(context).textTheme.display1,
              ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _updateAccessToken,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
      */